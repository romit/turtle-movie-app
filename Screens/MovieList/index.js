import React, { Component } from 'react'
import { View, Text, FlatList, StyleSheet } from 'react-native'

import MovieItem from '../../Components/MovieItem'

import { moviesService } from '../../services/'

export default class MovieList extends Component {
	
	constructor(props){
		super(props)

		this.state = {
			moviesList: [],
			isLoading: true
		}
	}

	componentDidMount(){
		moviesService.getAll()
			.then(
				movies => {
					this.setState({moviesList: movies, isLoading: false})
				},
				error => {
					console.log(error)
					this.setState({isLoading: false})
				}
			)
	}

	renderItem(movieDetails) {
	    return <MovieItem movie={movieDetails} onItemClick={(item) => this.props.navigation.navigate('MovieComments', { movie: item })}/>
	}

	renderHeader() {
	    return (
	        <Text style={styles.listHeaderText}>
	          Movies
	        </Text>
	    )
	}

	render() {
		return(
			<View style={styles.container}>
				{ this.state.moviesList.length === 0 &&
					<Text style={styles.loaderText}>
			          { this.state.isLoading ? 'Loading movies...' : 'No movies!'}
			        </Text>
				}
				{ this.state.moviesList.length !== 0 &&
					<FlatList
			            data={this.state.moviesList}
			            renderItem={({ item, index }) => this.renderItem(item, index)}
			            keyExtractor={(item, index) => index.toString()}
			            style={styles.movieListContainer}
			            showsVerticalScrollIndicator={true}
			            ListHeaderComponent={this.renderHeader()}
			        />
			    }
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		paddingTop: 30, 
		flex: 1	    
	},
	loaderText: {
		fontSize: 12, 
		marginTop: 30, 
		flex: 1, 
		alignSelf: 'center'
	},
	movieListContainer: { 
		width: '100%', 
		flex: 1, 
		paddingLeft: 12, 
		paddingRight: 12 
	},
	listHeaderText: {
		paddingTop: 16, 
		paddingBottom: 16, 
		fontSize: 24, 
		fontWeight: 'bold'
	}
})