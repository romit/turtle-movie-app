import React, { Component } from 'react'
import { View, Text, TextInput, ScrollView, StyleSheet, FlatList, KeyboardAvoidingView, Platform } from 'react-native'
import { Header } from 'react-navigation'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import MovieComment from '../../Components/MovieComment'
import Button from '../../Components/Button'

import { firebaseService } from '../../services/'

export default class MovieComments extends Component {

	constructor(props){
		super(props)

		this.state = {
			movieComment: '',
			comments: [],
			isLoading: true
		}

		this.submitComment = this.submitComment.bind(this)
	}

	static navigationOptions = ({ navigation }) => {
	    return {
	      title: navigation.getParam('movie').title,
	      headerTitleStyle: {
	        fontWeight: 'normal',
	        fontSize: 18
	      },
	    }
  	}

  	componentDidMount(){
  		firebaseService.fetchComments(this.props.navigation.getParam('movie').title)
  			.then(response => {
  				console.log('response is:' + response)
  				let comments = []
  				response.forEach((item) => {
  					console.log(item.val())
  					comments.push(item.val())
  				})
  				comments.reverse()
  				this.setState({comments: comments, isLoading: false})
  			})
  	}

  	submitComment(){	
  		let movieComment = this.state.movieComment
  		console.log(movieComment)

  		if(movieComment !== ''){
  			firebaseService
  				.writeComment(this.props.navigation.getParam('movie').title, movieComment)
  				.then(response => {
  					console.log(response)
  					this.setState({movieComment: ''})
  				})
  				.catch(error => {
  					console.log(error)
  				})
  		}

  		firebaseService.getMovieCommentsRef().once("child_added", (data, prevChildKey) => {
  			if(data.val().movieName === this.props.navigation.getParam('movie').title){
  				let oldComments = Object.assign([], this.state.comments)	
	  			oldComments.unshift(data.val())
	  			console.log(this.state)
	  			this.setState({comments: oldComments})
  			}
  		})
  	}

  	renderItem(movieComment) {
	    return <MovieComment comment={movieComment} />
	}

	renderHeader() {
	    return (
	        <Text style={styles.listHeaderText}>
	          Comments
	        </Text>
	    )
	}

	render() {
		
		const movie = this.props.navigation.getParam('movie')
		let keyboardVerticalOffset = Platform.OS === 'ios' ? Header.HEIGHT : Header.HEIGHT + 20
		
		return(
			<KeyboardAvoidingView style={{flex: 1}} behavior="padding" keyboardVerticalOffset={keyboardVerticalOffset}>
				{ this.state.comments.length === 0 &&
					<Text style={styles.loaderText}>
			          { this.state.isLoading ? 'Loading comments...' : 'No comments!'}
			        </Text>
				}
				{ this.state.comments.length !== 0 &&
					<FlatList
			            data={this.state.comments}
			            renderItem={({ item, index }) => this.renderItem(item, index)}
			            keyExtractor={(item, index) => index.toString()}
			            style={styles.movieCommentsContainer}
			            showsVerticalScrollIndicator={true}
			            ListHeaderComponent={this.renderHeader()}
			        />
				}
				<View style={styles.horizontalContainer}>
					<TextInput
				        style={styles.textInput}
				        onChangeText={(movieComment) => this.setState({movieComment})}
				        value={this.state.movieComment}
				        placeholder='Write comment'
			      	/>
			      	<Button onButtonClick={() => this.submitComment()} buttonItem={{text: 'SUBMIT', value: 'Submit'}}/>
				</View>
			</KeyboardAvoidingView>
		)
	}
}

const styles = StyleSheet.create({
	horizontalContainer: {
	    flexDirection: 'row',
	    justifyContent: 'space-between',
	    alignItems: 'center',
	    padding: 12,
	    backgroundColor: '#f9f9f9'
	},
	loaderText: {
		fontSize: 12, 
		marginTop: 30, 
		flex: 1, 
		alignSelf: 'center'
	},
	textInput: { 
		backgroundColor: '#fff', 
		height: 32, 
		paddingLeft: 12, 
		paddingRight: 12, 
		flex: 1, 
		borderColor: '#e5e5e5', 
		borderWidth: 1, 
		borderRadius: 16, 
		fontSize: 12
	},
	movieCommentsContainer: {
		width: '100%', 
		flex: 1, 
		paddingLeft: 12, 
		paddingRight: 12 
	},
	listHeaderText: {
		paddingTop: 16, 
		paddingBottom: 16, 
		fontSize: 24, 
		fontWeight: 'bold'
	}
})