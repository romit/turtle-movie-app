import axios from 'axios'

export const moviesService = {
	getAll
}

const apiUrl = 'https://tender-mclean-00a2bd.netlify.com/mobile/movies.json'

function getAll(){

	return axios.get(apiUrl)
		.then(response => {
			return response.data
		})
		.catch(error => {
			return Promise.reject(error);
		})
}