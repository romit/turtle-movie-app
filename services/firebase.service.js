import firebase from 'firebase'

export const firebaseService = {
	writeComment,
	fetchComments,
	getMovieCommentsRef
}

var config = {
    databaseURL: "https://moviechatapp-b608c.firebaseio.com",
    projectId: "moviechatapp-b608c",
}

if (!firebase.apps.length) {
    firebase.initializeApp(config)
}

var database = firebase.database()
const movieCommentsRef = database.ref('MovieComments/')

function writeComment(movieName, comment){
	var newCommentRef = movieCommentsRef.push()
	var timestamp = firebase.database.ServerValue.TIMESTAMP 
	var newMovieObj = {
		movieName,
		comment,
		timestamp
	}

	return newCommentRef.set(newMovieObj)
	.then(() => {
		console.log('added successfully')
		return newMovieObj
	})
	.catch(error => {
		return Promise.reject(error);
	})
}

function fetchComments(movieId){
	return movieCommentsRef.orderByChild("movieName").equalTo(movieId).once('value')
		.then(snapshot => {
		  return snapshot
		})
		.catch(error => {
			return Promise.reject(error);
		})
}

function getMovieCommentsRef(){
	return movieCommentsRef
}