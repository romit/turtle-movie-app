import React, { Component } from 'react'
import { View, TouchableHighlight } from 'react-native'

import Icon from '@expo/vector-icons/FontAwesome'
import IconEntypo from '@expo/vector-icons/Entypo'
import IconFeather from '@expo/vector-icons/Feather'
import IconMaterial from '@expo/vector-icons/MaterialIcons'
import IconMaterialComIcon from '@expo/vector-icons/MaterialCommunityIcons'
import IconIon from '@expo/vector-icons/Ionicons';

export default class IconButton extends Component {
	render() {

		let { iconType, iconName, iconSize, iconColor } = this.props

		return (
			<TouchableHighlight style={this.props.contentContainerStyle}
                onPress = { () => this.props.onButtonClick() } underlayColor='transparent'>
                <View>
                	{ iconType === 'fontAwesome' &&
                    	<Icon name={iconName} size={iconSize} color={iconColor} />
                	}
                	{ iconType === 'feather' &&
                    	<IconFeather name={iconName} size={iconSize} color={iconColor} />
                	}
                	{ iconType === 'entypo' &&
                    	<IconEntypo name={iconName} size={iconSize} color={iconColor} />
					}
					{ iconType === 'materialIcon' &&
							<IconMaterial name={iconName} size={iconSize} color={iconColor} />
					}
					{ iconType === 'ionIcon' &&
							<IconIon name={iconName} size={iconSize} color={iconColor} />
					}
					{ iconType === 'materialCommunityIcons' &&
							<IconMaterialComIcon name={iconName} size={iconSize} color={iconColor} />
					}
                </View>
            </TouchableHighlight>
		)
	}
}