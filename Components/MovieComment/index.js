import React, { Component } from 'react'
import { Text, View, TouchableHighlight, StyleSheet, Platform } from 'react-native'

import IconButton from '../IconButton'

export default class MovieComment extends Component {

  getLocalTimeFromUTC(utcTime){
      let time = new Date(utcTime).toString()
      var n = time.indexOf(":")
      var t = time.indexOf(" ", n)
      return time.substring(0, t)
  }

  render() {

    let { comment, timestamp } = this.props.comment

    return (
      <View style={styles.container}>
        <IconButton
          iconType='fontAwesome'
          iconName='user'
          iconSize={24}
          iconColor='#A6A6A6'
          contentContainerStyle={{ justifyContent: 'center', margin: 4, marginTop: 12, marginRight: 12 }}
          onButtonClick={() => {this.props.onItemClick(this.props.movie)}}
        />
        <View style={styles.verticalContainer}>
          <Text style={styles.titleText}>
            { comment }
          </Text>
          <Text style={styles.captionText}>
            {this.getLocalTimeFromUTC(timestamp)}
          </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  verticalContainer: {
    flexDirection: 'column',
    flex: 1,
    padding: 12,
    paddingTop: 4,
    paddingBottom: 12,
    backgroundColor: '#f9f9f9',
    borderRadius: 6,
    margin: 8,
    marginLeft: 2,
    marginRight: 2,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { height: 10, width: 0 },
        shadowOpacity: 1,
        shadowRadius: 14,
      },
      android: {
        elevation: 1,
      },
    })
  },
  titleText: {
    fontSize: 12,
    color: '#464646',
    marginBottom: 4,
    fontWeight: 'bold'
  },
  captionText: {
    fontSize: 11,
    marginTop: 2,
    color: '#999999'
  }
})