import React, { Component } from 'react'
import { View, Text, TouchableHighlight, StyleSheet, Platform } from 'react-native'

export default class Button extends Component {
	render() {

		const { buttonItem } = this.props

		return (
			<TouchableHighlight
          onPress={() => this.props.onButtonClick()}
          underlayColor='transparent'
          style={this.props.contentContainerStyle}
          >
            <View style={[styles.button, this.props.buttonStyle]}>
              <Text style={[styles.buttonText, this.props.buttonTextStyle]}>
                  { buttonItem.text } 
              </Text>
            </View>
      </TouchableHighlight>
		)
	}
}

const styles = StyleSheet.create({
  button: {
    padding: 6,
    paddingLeft: 16,
    paddingRight: 16,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    margin: 4,
    backgroundColor: '#2096F3',
    borderColor: '#fff',
    ...Platform.select({
        ios: {
          shadowColor: 'rgba(0, 0, 0, 0.04)',
          shadowOffset: { height: 10, width: 0 },
          shadowOpacity: 1,
          shadowRadius: 14,
        },
        android: {
          elevation: 1,
        },
      }),
  },
  buttonText: {
    fontSize: 12,
    textAlign: 'center',
    color: '#fff',
  }
})