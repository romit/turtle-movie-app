import React, { Component } from 'react'
import { Text, View, TouchableHighlight, StyleSheet, Platform } from 'react-native'

import IconButton from '../IconButton'

export default class MovieItem extends Component {
  render() {

    let { title, year, rating } = this.props.movie

    return (
      <TouchableHighlight onPress={() => this.props.onItemClick(this.props.movie)} underlayColor='#00000000'>
        <View style={styles.container}>
          <View style={styles.verticalContainer}>
            <Text style={styles.titleText}>
              { title }
            </Text>
            <Text style={styles.captionText}>
              {year}
            </Text>
          </View>
          <IconButton
            iconType='fontAwesome'
            iconName='angle-right'
            iconSize={26}
            iconColor='#A6A6A6'
            contentContainerStyle={{ alignItems: 'center', justifyContent: 'center', margin: 4 }}
            onButtonClick={() => {this.props.onItemClick(this.props.movie)}}
          />
        </View>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 12,
    paddingTop: 12,
    paddingBottom: 12,
    backgroundColor: '#f9f9f9',
    borderRadius: 6,
    margin: 8,
    marginLeft: 2,
    marginRight: 2,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { height: 10, width: 0 },
        shadowOpacity: 1,
        shadowRadius: 14,
      },
      android: {
        elevation: 1,
      },
    })
  },
  verticalContainer: {
    flexDirection: 'column',
    flex: 1
  },
  titleText: {
    fontSize: 18,
    color: '#464646',
    marginBottom: 4,
  },
  amountText: {
    fontSize: 40,
    color: '#464646'
  },
  captionText: {
    fontSize: 12,
    color: '#999999'
  }
})