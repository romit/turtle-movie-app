import React from 'react';
import { Text, View } from 'react-native';

import AppStack from './config/routes'

export default class App extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <AppStack />
      </View>
    );
  }
}
