import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import MovieList from '../Screens/MovieList'
import MovieComments from '../Screens/MovieComments'

const AppNavigator = createStackNavigator({
  MovieList: {
    screen: MovieList,
    navigationOptions: {
      header: null
    }
  },
  MovieComments: {
  	screen: MovieComments
  }
});

export default createAppContainer(AppNavigator);