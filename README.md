## How to run

To run the project, clone the repo in your system

and then change dir to cloned repo

You need to install expo-cli to run the project, and expo app on your device

To install expo-cli run `npm install -g expo-cli`

After installing expo-cli, run `npm install` inside the cloned repo

then run `expo start` inside the cloned repo and open the app in the expo app using the QR code


## App Architecture

App architecture is majorly divided into below parts 

    - Components
    - Services
    - Config
    - Screens
    
Components consists of reusable ui blocks

Services consists of rest api calls and firebase service

Config consists of navigation handlers and other top level config parameters

Screens is made up of Components, and uses both Config and Services


## Scope of Improvement

- I would add pagination in both the lists, movie lists and comments
- Better architecture, maybe use some state management solution such as redux
- Add username storage ability for commenting purpose
